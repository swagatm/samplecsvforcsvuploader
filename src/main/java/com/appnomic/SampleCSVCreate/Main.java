package com.appnomic.SampleCSVCreate;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Main {
    public static int MaxValue;
    public static int MinValue;
    public static void main(String[] args) {
        JSONArray hostKPIs = readFile(args[0]);
        JSONArray instKPIs = readFile(args[1]);
        JSONObject configData = readConfig(args[2]);
        MaxValue = Integer.parseInt((String) configData.get("MaxSampleValue"));
        MinValue = Integer.parseInt((String) configData.get("MinSampleValue"));
        String resultPath = args[3];
        ArrayList<String> time = timeConversionfromString(configData);
        for(int i =0;i<((JSONArray)configData.get("hostFileName")).size();i++){
            ArrayList<String> hostKpidata = new ArrayList<>();
            convertToArrayList(hostKPIs,hostKpidata);
            hostKpidata.add(0,"Timestamp");
            ArrayList<String[]> hostCsvData = new ArrayList<>();
            makeCSV(hostKpidata,hostCsvData,time);
            printData(hostCsvData);
            String fileName = (String) ((JSONArray)configData.get("hostFileName")).get(i);
            writeDataToFile(resultPath,fileName,hostCsvData);
        }
        for(int i =0;i<((JSONArray)configData.get("instFileName")).size();i++){
            ArrayList<String> instKpidata = new ArrayList<>();
            convertToArrayList(instKPIs,instKpidata);
            instKpidata.add(0,"Timestamp");
            ArrayList<String[]> instCsvData = new ArrayList<>();
            makeCSV(instKpidata,instCsvData,time);
            printData(instCsvData);
            String fileName = (String) ((JSONArray)configData.get("instFileName")).get(i);
            writeDataToFile(resultPath,fileName,instCsvData);
        }
    }

    private static void writeDataToFile(String resultPath, String fileName, ArrayList<String[]> CsvData) {
        try {
            FileWriter fr = new FileWriter(resultPath+""+fileName+".csv");
            for(int i =0;i<CsvData.size();i++){
                for(int j =0;j<CsvData.get(i).length-1;j++){
                    fr.write(CsvData.get(i)[j]+",");
                }
                fr.write(CsvData.get(i)[CsvData.get(i).length-1]);
                fr.write("\n");
            }
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void printData(ArrayList<String[]> csvData) {
        for(int i =0;i<csvData.size();i++){
            System.out.println(Arrays.toString(csvData.get(i)));
        }
        System.out.println(csvData.get(1).length);
    }

    //Arraylist of string converted to arraylist of String[]
    private static void makeCSV(ArrayList<String> kpidata, ArrayList<String[]> csvData, ArrayList<String> time) {
        String[] a = kpidata.toArray(new String[0]);
        csvData.add(a);
        for(int j=0;j<time.size();j++){
            ArrayList<String> str = new ArrayList<>();
            for(int i =1;i<csvData.get(0).length;i++){
                str.add(i-1,getRandomNumber());
            }
            str.add(0,time.get(j));
            String[] a1 = str.toArray(new String[0]);
            csvData.add(j+1,a1);
        }


//        for(int i =0;i<time.size();i++){
//            csvData.get(i+1)[0] = time.get(i);
//        }
    }

    private static String getRandomNumber() {
        double d = MinValue + (Math.random() * ((MaxValue - MinValue) + 1));
        double a = (double) Math.round(d * 100) / 100;
        return String.valueOf(a);
    }

    //JSONArry to ArrayList of String
    private static void convertToArrayList(JSONArray kpis, ArrayList<String> kpidata) {
        for (int i =0;i<kpis.size();i++){
            kpidata.add((String) kpis.get(i));
        }
    }
    private static JSONArray readFile(String arg) {
        String ConfigFile = arg;
        JSONParser jsonParser = new JSONParser();
        JSONArray jsonObject = null;
        try {
            FileReader fr = new FileReader(ConfigFile);
            jsonObject = (JSONArray) jsonParser.parse(fr);
            fr.close();
        } catch (org.json.simple.parser.ParseException | IOException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
    private static JSONObject readConfig(String arg) {
        String ConfigFile = arg;
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = null;
        try {
            FileReader fr = new FileReader(ConfigFile);
            jsonObject = (JSONObject) jsonParser.parse(fr);
            fr.close();
        } catch (org.json.simple.parser.ParseException | IOException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
    public static ArrayList<String> timeConversionfromString(JSONObject configData) {
        String timeinString = (String) configData.get("startTime");
        String timeFormat = (String) configData.get("timestampFormat");
        int totalTimeInHr = Integer.parseInt((String) configData.get("totalTimeInHr"));
        int timeInMin = totalTimeInHr*60;
        Date temp = null;
        SimpleDateFormat df = new SimpleDateFormat(timeFormat);
        try {
            temp = df.parse(timeinString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(temp);
        ArrayList<String> time = new ArrayList<>();
        time.add(0,timeinString);
        for(int i=0; i<timeInMin; i++){
            cal.add(Calendar.MINUTE, 1);
            String newTime = df.format(cal.getTime());
            time.add(i+1,newTime);
        }
        return time;
    }

}
